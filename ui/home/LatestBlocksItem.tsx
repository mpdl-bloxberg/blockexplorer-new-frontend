import {
  Box,
  Flex,
  Grid,
  Skeleton,
  Image,
} from '@chakra-ui/react';
import { motion } from 'framer-motion';
import React from 'react';

import type { Block } from 'types/api/block';

import config from 'configs/app';
// import getBlockTotalReward from 'lib/block/getBlockTotalReward';
import getNetworkValidatorTitle from 'lib/networks/getNetworkValidatorTitle';
import BlockTimestamp from 'ui/blocks/BlockTimestamp';
import AddressEntity from 'ui/shared/entities/address/AddressEntity';
import BlockEntity from 'ui/shared/entities/block/BlockEntity';

type Props = {
  block: Block;
  isLoading?: boolean;
}

const truncateString = (str: string | Array<any>, maxLength: number) => {
  if (str.length <= maxLength) {
    return str;
  }
  return `${ str.slice(0, maxLength - 3) }...`; // Truncate with ellipsis (...)
};

const getLogoAndNameByMiner = (miner: string) => {
  miner = miner.toLowerCase();
  switch (miner) {
    case '0xc604ffa8ade14dc9a22b6b19bdfc07e489156e53':
      return { logo: 'static/AGAP_Institute.png', name: 'AGAP Institute' };
    case '0x67c7448172cf110dc7b0c44a40027ef27f037c60':
      return { logo: 'static/Auckland_University_of_Technology.png', name: 'Auckland University of Technology' };
    case '0x2eaea0039c54f63cc344c3eaacfe69421c7ee785':
      return { logo: 'static/Bogazici_University.png', name: 'Bogazici University' };
    case '0x2fa6f57ff56a1da41fb7c6a176f630641e20cd53':
      return { logo: 'static/Carnegie_Mellon_University.png', name: 'Carnegie Mellon University' };
    case '0xbf46df3ee75f8aabed8a1954a386ff50880fe1d3':
      return { logo: 'static/Centre_for_Distributed_Ledger_Technologies.png', name: 'University of Malta - Centre for Distributed Ledger Technologies' };
    case '0xc636283c5d8d1f11fa51fa04b5412517c4de7ac9':
      return { logo: 'static/Chiba_Institute_of_Technology.png', name: 'Chiba Institute of Technology' };
    case '0x0fc31d48c40d652f32322d3d89c5d8a67b37b3be':
      return { logo: 'static/Cyril_University.png', name: 'Cyril University' };
    case '0x55299871982df69fa3d2b3770e25a33ca4ffd8fd':
      return { logo: 'static/Faculty_of_Organization_and_Informatics.png', name: 'University of Zagreb, Faculty of Organization and Informatics (FOI)' };
    case '0x841c25a1b2ba723591c14636dc13e4deeb65a79b':
      return { logo: 'static/Faculty_Sciences.png', name: 'University of Belgrade / Faculty of Organizational Sciences' };
    case '0x3869bc0fbe9e69ec32dbb66e1d3a2bd74399d273':
      return { logo: 'static/Fairfield_Dolan.png', name: 'Fairfield Dolan' };
    case '0x9b522bca5caffd5684d8498e04f1e3dc4735d109':
      return { logo: 'static/Ferdinand_Institute.png', name: 'Ferdinand Institute' };
    case '0x63fb9aab8bcd1837d8d1a5318b73b1d4adc4fc6a':
      return { logo: 'static/Fraunhofer_Institute.png', name: 'Fraunhofer Institute' };
    case '0x738e6e88d4415e2e5075e15cc24fd9416f1c89c3':
      return { logo: 'static/Furtwangen_University.png', name: 'Hochschule Furtwangen (HFU)' };
    case '0xffc32fb02c5d227480246951a75eb00b18df3f1f':
      return { logo: 'static/Gakushuin_University.png', name: 'Gakushuin University' };
    case '0x8de281f47b137979e55b6cea598179737574c774':
      return { logo: 'static/Georgia_Technology.png', name: 'Georgia Technology' };
    case '0x5385e5f2eeca3aa0d4671d166f0ebb05ba20d710':
      return { logo: 'static/Goethe_University_Frankfurt.png', name: 'Goethe University Frankfurt' };
    case '0xf0f659e9ec6b4358a8d7fba6a0ca79baade10552':
      return { logo: 'static/Institute_for_Internet_Security.png', name: 'Westfälische Hochschule; Institute for Internet Security (ifis)' };
    case '0xaa4870919390f1026c17651b4f8f29cbc50fd789':
      return { logo: 'static/Instituto_Gulbenkian.png', name: 'Instituto Gulbenkian de Ciencia' };
    case '0x643950742ec05fb0ebe2af75af99d811e0c255f2':
      return { logo: 'static/ircelyon.png', name: 'ircelyon' };
    case '0xe659bc6a60ba2091c08f7df623ba6057349b6980':
      return { logo: 'static/IT_University_of_Copenhagen.png', name: 'IT University of Copenhagen' };
    case '0x8823a9e567bac419c394c646e1d2f0929d2039ee':
      return { logo: 'static/Karlsruhe_Institute_of_Technology.png', name: 'Karlsruhe Institute of Technology' };
    case '0xa2a417f9133667a24a8be4fac41f3e3534842e20':
      return { logo: 'static/Ludwig_Maximilians_Universität_Munich.png', name: 'Ludwig Maximilians Universität Munich' };
    case '0xaa84378fa41da83a9b6523ba46e45a664fbebfc8':
      return { logo: 'static/max_planck.png', name: 'Max planck institute' };
    case '0x8fb88d387a757f22f54411d822dd1d3557b505ca':
      return { logo: 'static/Mendel_University.png', name: 'Mendel University' };
    case '0xde564bf2d9885fbfde5556f2ca4a72f1fc0ac307':
      return { logo: 'static/National_Defence_University.png', name: 'Carol I National Defense University' };
    case '0xf824831114be0c8b3819077c8fd514474672fc8b':
      return { logo: 'static/School_of_Economics_Sarajevo.png', name: 'University of Sarajevo, School of Economics' };
    case '0x61aa0b556e23a28b1c634b97307c20e048c2f116':
      return { logo: 'static/Simon_Business_School.png', name: 'Simon Business School, University of Rochester' };
    case '0xee4308865d6b23afd70b7108a35dea8d6481bbc2':
      return { logo: 'static/The_Open_University.png', name: 'The Open University' };
    case '0x12c5ff506ffacf2056f04f3889f45e4187df98f2':
      return { logo: 'static/Universidade_Parana.png', name: 'Universidade Tecnológica Federal do Paraná' };
    case '0xa59ddc352a6521ef4cb338dae4b1b2a609233194':
      return { logo: 'static/University_College_London.png', name: 'University College London' };
    case '0x28570e7acff6c62270e08b639cc7724dd728035f':
      return { logo: 'static/University_di_Torino.png', name: 'University di Torino' };
    case '0x6ed81c13b52cb7e64ca29bbc726b1b2c0b685181':
      return { logo: 'static/University_of_Ansbach.png', name: 'University of Ansbach' };
    case '0x37d1b7a3eec870e593c32d790e52b89908ca90f1':
      return { logo: 'static/University_of_Geneva.png', name: 'University of Geneva' };
    case '0x93da5507a26090448a03fc1f77e1c7da20a24292':
      return { logo: 'static/University_of_Johannesburg.png', name: 'University of Johannesburg' };
    case '0xdddcb89201f5a24891610b033351a5408a081f98':
      return { logo: 'static/University_of_Kassel.png', name: 'University of Kassel' };
    case '0x6e7215893131bf41af6256b5cf0bd61bd631b796':
      return { logo: 'static/University_of_Nicosia.png', name: 'University of Nicosia' };
    case '0xe845ccc925b44c2f50ce8e67103dfe840efa8f89':
      return { logo: 'static/University_of_Split.png', name: 'University of Split' };
    case '0x9ca00a5b1b61157d5d75f0c41cddb157dd050d71':
      return { logo: 'static/University_of_Technikum_Wien.png', name: 'University of Technikum Wien' };
    case '0xa5f9bfd31ade231fe768cdc65567e0d60eeb556a':
      return { logo: 'static/University_of_Timisoara.png', name: 'West University of Timisoara' };
    case '0x65dc5ce59b15250b8acdd90c3c171e2e7f05d9b7':
      return { logo: 'static/University_of_West_Attica.png', name: 'University of West Attica' };
    case '0xc9d13d9b510b73c63865ec1a19de5462c8a4e053':
      return { logo: 'static/UZH_Blockchain_Center.png', name: 'University of Zurich / UZH Blockchain Center' };
    case '0xd96c56ab670b8dbb213cf0331433d47221f169c5':
      return { logo: 'static/Weizenbaum.png', name: 'Weizenbaum' };
    case '0x548cc5805b67c181913cc88fde3a70e37a0718ce':
      return { logo: 'static/West_University_of_Timișoara.png', name: 'West University of Timișoara' };
    case '0x28afcdeb1bebd091b4526f278cb588ae6637a622':
      return { logo: 'static/Wroclaw_University.png', name: 'Wroclaw University' };
    case '0x008d897edcfaa9be1e4acfbd4d8659bb0d33f1a0':
      return { logo: 'static/ZIBS.png', name: 'Zhejiang University(ZIBS)' };
    case '0x478927dfc20e4a999650391d917022a5c81df0bd':
      return { logo: 'static/university_bremen.png', name: 'University of Bremen' };
    case '0xb612bbb8bccf85f9c3e7d0997990a89a0580600e':
      return { logo: 'static/CenterForInfo.png', name: 'Center for Informatics and Computing Science Ruđer Bošković Institute Zagreb' };
    default:
      return { logo: 'static/bloxberg_Not_found_logo.png', name: '' };
  }
};

const LatestBlocksItem = ({ block, isLoading }: Props) => {
  // const totalReward = getBlockTotalReward(block);
  const allInfo = getLogoAndNameByMiner(block.miner.hash);

  const truncatedName = truncateString(allInfo.name, 40); // Adjust max length as needed

  return (
    <Box
      as={ motion.div }
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ display: 'none' }}
      transitionDuration="normal"
      transitionTimingFunction="linear"
      border="1px solid"
      borderColor="divider"
      p={ 2 }
    >
      <Flex alignItems="center" overflow="hidden" w="100%" mb={ 0 } >
        <BlockEntity
          isLoading={ isLoading }
          number={ block.height }
          tailLength={ 2 }
          fontSize="xl"
          lineHeight={ 7 }
          fontWeight={ 500 }
          mr="auto"
        />
        <BlockTimestamp
          ts={ block.timestamp }
          isEnabled={ !isLoading }
          isLoading={ isLoading }
          fontSize="sm"
          flexShrink={ 0 }
          ml={ 2 }
        />
      </Flex>
      <Flex alignItems="center" overflow="hidden" w="100%" >
        <Grid gridGap={ 1 } templateColumns="auto minmax(0, 1fr)" fontSize="sm">
          <Skeleton isLoaded={ !isLoading }>Txn</Skeleton>
          <Skeleton isLoaded={ !isLoading } color="text_secondary"><span>{ block.tx_count }</span></Skeleton>

          { /* { !config.features.rollup.isEnabled && !config.UI.views.block.hiddenFields?.total_reward && (
            <>
              <Skeleton isLoaded={ !isLoading }>Reward</Skeleton>
              <Skeleton isLoaded={ !isLoading } color="text_secondary"><span>{ totalReward.dp(10).toFixed() }</span></Skeleton>
            </>
          ) } */ }

          { !config.features.rollup.isEnabled && !config.UI.views.block.hiddenFields?.miner && (
            <>
              <Skeleton isLoaded={ !isLoading } textTransform="capitalize">{ getNetworkValidatorTitle() }</Skeleton>
              <AddressEntity
                address={ block.miner }
                isLoading={ isLoading }
                noIcon
                noCopy
                truncation="constant_long"
              />
            </>
          ) }

          <Skeleton isLoaded={ !isLoading }>Validator Name</Skeleton>
          <Skeleton isLoaded={ !isLoading } color="blue.300"><span>{ truncatedName }</span></Skeleton>

        </Grid>
        <Flex justifyContent="center" alignItems="center" ml="auto">
          <Image
            src={ allInfo.logo }
            alt="Miner Logo"
            width="90px"
            height="90px"
          />
        </Flex>
      </Flex>

    </Box>
  );
};

export default LatestBlocksItem;
