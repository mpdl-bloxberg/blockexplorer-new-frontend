import { Box, Flex } from '@chakra-ui/react';
import React from 'react';

import config from 'configs/app';
import ChainIndicators from 'ui/home/indicators/ChainIndicators';
import LatestBlocks from 'ui/home/LatestBlocks';
import LatestZkEvmL2Batches from 'ui/home/LatestZkEvmL2Batches';
import Stats from 'ui/home/Stats';
import Transactions from 'ui/home/Transactions';
import SearchBar from 'ui/snippets/searchBar/SearchBar';

const rollupFeature = config.features.rollup;

const Home = () => {
  return (
    <Box as="main">
      <SearchBar isHomepage/>
      <Stats/>
      <Flex mt={ 5 } columnGap={ 5 } >
        <Box>
          <ChainIndicators/>
          <Box mt={ 3 }>
            <Transactions/>
          </Box>
        </Box>
        <Box flexGrow={ 1 }>
          { rollupFeature.isEnabled && rollupFeature.type === 'zkEvm' ? <LatestZkEvmL2Batches/> : <LatestBlocks/> }
        </Box>
      </Flex>
    </Box>
  );
};

export default Home;
